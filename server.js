

console.clear();

// Include modules
let express = require('express');
let app = express();
let mysql = require('mysql');
let bodyParser = require('body-parser');

// Server configuration settings
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:true }));

// Database settings
let conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'reservations'
});

//host = '192.168.0.103';
host = 'localhost'

let serverConf = {
    port: 3000,
    host: host
}

// Start server
app.listen(serverConf);

// Test database connection
conn.query('SHOW TABLES', function(err,res) {
    if (err) throw err;
        console.log('database connection is working!');
});

// Headers for every module
// Usage SQL statements, forms, components
function getHeaders(cat) {
    switch (cat) {
        case 'branch':
            return ['BranchID', 'Branch_Name', 'Branch_Address', 'Branch_Phone', 'Branch_Email'];
        case 'branches':
            return ['BranchID', 'Branch_Name', 'Branch_Address', 'Branch_Phone', 'Branch_Email'];
        case 'accommodation':
            return ['AccommodationID', 'Nights', 'CheckIn', 'CheckOut', 'CabinID', 'ReservationID'];
        case 'cabin':
            return [ 'CabinID', 'Cabin_Name', 'Cabin_Price', 'BranchID', 'Cabin_TypeID' ];
        case 'cabin_type':
            return [ 'Cabin_TypeID', 'Type_Name', 'Party_Size' ];
        case 'customer':
            return [ 'CustomerID', 'Courtesy_Title', 'First_Name', 'Last_Name', 'SSN', 'Street_Address', 'City', 'ZIP', 'Country', 'Phone', 'Email' ];
        case 'invoice':
            return [ 'InvoiceID', 'ReservationID', 'Invoice_Total' ];
        case 'rendered':
            return [ 'RenderedID', 'Rendered_Date', 'Service_Amount', 'ServiceID', 'ReservationID' ];
        case 'reservation':
            return [ 'ReservationID', 'Reservation_Date', 'CustomerID' ];
        case 'service':
            return [ 'ServiceID', 'Service_Name' ];
        case 'site':
            return [ 'SiteID', 'Service_Price', 'ServiceID', 'BranchID' ];
    }
}

// Keys for every module
// Usage SQL statements, forms, components
function getKeys(cat) {
    switch (cat) {
        case 'branch':
            return ['BranchID', 'Branch_Name', 'Branch_Address', 'Branch_Phone', 'Branch_Email'];
        case 'branches':
            return ['BranchID', 'Branch_Name', 'Branch_Address', 'Branch_Phone', 'Branch_Email'];
        case 'accommodation':
            return ['AccommodationID', 'Nights', 'CheckIn', 'CheckOut', 'CabinID', 'ReservationID'];
        case 'cabin':
            return [ 'CabinID', 'Cabin_Name', 'Cabin_Price', 'BranchID', 'Cabin_TypeID' ];
        case 'cabin_type':
            return [ 'Cabin_TypeID', 'Type_Name', 'Party_Size' ];
        case 'customer':
            return [ 'CustomerID', 'Courtesy_Title', 'First_Name', 'Last_Name', 'SSN', 'Street_Address', 'City', 'ZIP', 'Country', 'Phone', 'Email' ];
        case 'invoice':
            return [ 'InvoiceID', 'ReservationID', 'Invoice_Total' ];
        case 'rendered':
            return [ 'RenderedID', 'Rendered_Date', 'Service_Amount', 'ServiceID', 'ReservationID' ];
        case 'reservation':
            return [ 'ReservationID', 'Reservation_Date', 'CustomerID' ];
        case 'service':
            return [ 'ServiceID', 'Service_Name' ];
        case 'site':
            return [ 'SiteID', 'Service_Price', 'ServiceID', 'BranchID' ];
    }
}



//################## SERVER ROUTING ##################//
// Send index.html to client
app.get('/', function(req,res) {
    res.sendFile(__dirname + '/index.html');
});


// Management route
// Prepare management list data and send it to client
app.get('/api/management/:cat/', function(req,res) {
    let category = req.params.cat;
    let id = req.query.id;

    management = Management(category, id);
    management.then(function(result) {
        res.json(result);
    });
});

// Edit route
// Prepare form with data if ID is given
// Send form data to client
app.get('/api/edit/:cat/', function(req,res) {
    let category = req.params.cat;
    let id = req.query.id;

    edit = EditItem(category, id);
    edit.then(function(result) {
        res.json(result);
    });
});

// Form route
// Parse received data and push the update/ insert into database
app.post('/api/edit/:cat/', function(req,res) {
    category = req.params.cat;
    let data = req.body;
    let isNew = req.query.new;

    console.log( category );

    handleForm = FormHandler(category, data.data, isNew);
    handleForm.then(function(result) {

        res.send( result );

    }, function(err) {
        console.log(err);
        res.send(err);
    });
});

// Delete route
// Delete record from database
app.post('/api/delete/:cat', function(req,res) {
    let category = req.params.cat;
    let id = req.query.id;

    maria = Maria(prepareSQLDelete(category, id));
    maria.then(function(result) {
        res.send( result );
    });
});

// Component route
app.get('/api/component/:cat', function(req,res) {
    let category = req.params.cat;
    let id = req.query.id;

    component = Component(category, id);
    component.then(function(result) {
        res.send(result);
    });
});

// Report route
app.get('/api/report/', function(req,res) {
    category = req.query.cat;
    branchID = req.query.id;
    fromDate = req.query.from;
    toDate = req.query.to;
    ftDate = { from:fromDate, to:toDate };

    report = Report(category, branchID, ftDate);
    report.then(function(result) {
        res.json(result);
    });
});

// Invoice route

app.get('/api/invoice/', function(req,res) {
    resID = req.query.id;

    invoice = Invoice(resID);
    invoice.then(function(result) {

        res.json( result );
    });
});

app.post('/api/updateInvoice', function(req,res) {
    console.log( req.body );

    maria = Maria('UPDATE invoice SET Invoice_Total='+req.body.total+' WHERE ReservationID='+req.body.ReservationID);
    maria.then(function(result) {
        res.send( result );
    });
});

app.get('/api/search/:cat', function(req,res) {
    let category = req.params.cat;
    let key = req.query.key;
    let value = req.query.value;

    listSearch = ListSearch( category, key, value );
    listSearch.then(function(result) {
        res.send( result );
    });
});

app.get('/api/print', function(req,res) {

    res.send('');
});


//################## SEARCH LIST STARTS ##################//
function ListSearch( cat, key, value ) {
    return new Promise(function(resolve,reject) {

        //sql = 'SELECT * FROM '+cat+' WHERE '+key+'="'+value+'";';

        //sql = 'SELECT * FROM '+cat+' WHERE '+key+' LIKE \'"'+value+'"%\'';

        sql = 'SELECT * FROM '+cat+' WHERE '+key+' LIKE \''+value+'%\''

        body = {
            category: cat,
            headers: getHeaders(cat),
            keys: getKeys(cat),
            data: [],
        }

        maria = Maria(sql);
        maria.then(function(result) {
            
            body.data = result;
            resolve(body);
        });
    });
}



//################## SEARCH LIST STARTS ##################//


//################## SERVER ROUTING ENDS ##################//
//################## INVOICE STARTS ##################//
function Invoice(resID) {
    return new Promise(function(resolve,reject) {

        body = {
            accommodation: [],
            services: [],
            titles: [ 'Reservation summary', 'Customer info', 'Branch info' ],
            customer: {
                data: [],
                keys: [ 'CustomerID', 'First_Name', 'Last_Name', 'Street_Address', 'Phone', 'Email', 'ZIP' ],
            },
            branchKeys: [ 'Branch_Name', 'Branch_Address', 'Branch_Phone', 'Branch_Email' ],
            reservationKeys: [ 'Reservation_Date', 'ReservationID' ],
            invoiceKeys: [ 'Cabin_Name', 'Cabin_Price', 'Nights' ],
            serviceKeys: [ 'Service_Name', 'Service_Price', 'Service_Amount' ],
        }

        accommodation = getAccommodations(resID, 'accommodation');
        accommodation.then(function(result) {
            body.accommodation = result;

                service = getServices(resID, 'rendered');
                service.then(function(result) {
                    body.services = result;

                        customer = getCustomerDetails(resID);
                        customer.then(function(result) {
                            body.customer.data = result;

                            resolve( body );


                        });
                });
        });

    });
}


function getAccommodations(resID, cat) {
    return new Promise(function(resolve,reject) {

        sql = 'SELECT d.*, e.*, a.*, c.CustomerID, c.First_Name, c.Last_Name FROM accommodation AS a'+
        ' INNER JOIN reservation AS b ON a.ReservationID=b.ReservationID'+
        ' INNER JOIN customer AS c ON b.CustomerID=c.CustomerID'+
        ' INNER JOIN cabin AS d ON a.CabinID=d.CabinID'+
        ' INNER JOIN branch AS e ON d.BranchID=e.BranchID'+
        ' WHERE b.ReservationID="'+resID+'";';

        //'SELECT * FROM '+cat+' WHERE reservationID='+resID

        maria = Maria(sql);
        maria.then(function(result) {
            resolve(result);
        });
    });
}
function getServices(resID, cat) {
    return new Promise(function(resolve,reject) {

        sql = 'SELECT * FROM rendered AS a'+
        ' INNER JOIN reservation AS b ON a.ReservationID=b.ReservationID'+
        ' INNER JOIN accommodation AS c ON b.ReservationID=c.ReservationID'+
        ' INNER JOIN cabin AS d ON d.CabinID=c.CabinID'+
        ' INNER JOIN branch AS e ON e.BranchID=d.BranchID'+
        ' INNER JOIN site AS f ON e.BranchID=f.BranchID'+
        ' INNER JOIN customer AS g ON g.CustomerID=b.CustomerID'+
        ' INNER JOIN service AS h ON h.ServiceID=a.ServiceID'+
        ' WHERE b.ReservationID='+resID+''+
        ' GROUP BY a.renderedID'

        //'SELECT * FROM '+cat+' WHERE reservationID='+resID;

        maria = Maria(sql);
        maria.then(function(result) {
            resolve(result);
        });
    });
}
function getCustomerDetails(resID) {
    return new Promise(function(resolve,reject) {
        maria = Maria('SELECT * FROM reservation AS a INNER JOIN customer AS b ON a.CustomerID=b.CustomerID WHERE a.ReservationID='+resID);
        maria.then(function(result) {
            resolve(result);
        });
    });
}

//################## INVOICE ENDS ##################//



//################## PREPARE REPORT STARTS ##################//
// Select all items from database where branch ID = branchID
//
// NEEDS WORK!!!
//
//
function Report(cat, branchID, ftDate) {
    return new Promise(function(resolve,reject) {

        if ( ftDate.from == '' ) {
            ftDate.from = '0000-00-00'
        }

        if ( ftDate.to == '' ) {
            ftDate.to = '9000-00-00'
        }


        switch (cat) {
            case 'accommodation':
//                sql = 'SELECT * FROM '+cat+' WHERE CheckIn>"'+ftDate.from+'" AND CheckOut<"'+ftDate.to+'"';

                    sql = 'SELECT a.*, d.Cabin_Name, c.CustomerID, c.First_Name, e.Branch_Name, c.Last_Name FROM accommodation AS a'+
                        ' INNER JOIN reservation AS b ON a.ReservationID=b.ReservationID'+
                        ' INNER JOIN customer AS c ON b.CustomerID=c.CustomerID'+
                        ' INNER JOIN cabin AS d ON a.CabinID=d.CabinID'+
                        ' INNER JOIN branch AS e ON d.BranchID=e.BranchID'+
                        ' WHERE a.CheckIn>"'+ftDate.from+'" AND a.CheckOut<"'+ftDate.to+'"';

                    if ( branchID != 'Select branch') {
                        sql +=  'AND e.BranchID="'+branchID+'"';
                    }

                    body = {
                        category: cat,
                        data: [],
                        keys: [ 'AccommodationID', 'Cabin_Name', 'Nights', 'CheckIn', 'CheckOut', 'CabinID', 'ReservationID', 'Branch_Name', 'CustomerID', 'First_Name', 'Last_Name', 'options' ],
                    }

                break;
// DOES NOT WORK!!!
// Needs to be fixed!
            case 'rendered':


            sql = 'SELECT * FROM rendered AS a'+
                    ' INNER JOIN reservation AS b ON a.ReservationID=b.ReservationID'+
                    ' INNER JOIN accommodation AS c ON b.ReservationID=c.ReservationID'+
                    ' INNER JOIN cabin AS d ON d.CabinID=c.CabinID'+
                    ' INNER JOIN branch AS e ON e.BranchID=d.BranchID'+
                    ' INNER JOIN site AS f ON e.BranchID=f.BranchID'+
                    ' INNER JOIN customer AS g ON g.CustomerID=b.CustomerID'+
                    ' INNER JOIN service AS h ON h.ServiceID=a.ServiceID'+
                    ' WHERE a.Rendered_Date>"'+ftDate.from+'" AND a.Rendered_Date<"'+ftDate.to+'"';
                    

            /*
                    sql = 'SELECT a.Rendered_Date, a.Service_Amount, c.CustomerID, c.First_Name, c.Last_Name, d.Service_Name, e.Service_Price FROM rendered AS a'+
                        ' INNER JOIN reservation AS b ON a.ReservationID=b.ReservationID'+
                        ' INNER JOIN customer AS c ON b.CustomerID=c.CustomerID'+
                        ' INNER JOIN service AS d ON a.ServiceID=d.ServiceID'+
                        ' INNER JOIN site AS e ON d.ServiceID=e.ServiceID'+
                        ' INNER JOIN branch AS f ON e.BranchID=f.BranchID'+
                        ' WHERE a.Rendered_Date>"'+ftDate.from+'" AND a.Rendered_Date<"'+ftDate.to+'"';

*/

                        if ( branchID != 'Select branch') {
                            sql +=  ' AND f.BranchID="'+branchID+'"';
                        }

                    sql += ' GROUP BY renderedID'


                    body = {
                        category: cat,
                        data: [],
                        keys: [ 'RenderedID', 'ReservationID', 'Rendered_Date', 'CustomerID', 'First_Name', 'Last_Name', 'Branch_Name', 'Service_Amount', 'Service_Name', 'Service_Price', 'options' ],
                    }

/*
                'SELECT service.*, rendered.*, reservation.Reservation_Date, customer.CustomerID, customer.First_Name, customer.Last_Name'+
                ' FROM (((service'+
                ' INNER JOIN rendered ON service.ServiceID = rendered.ServiceID)'+
                ' INNER JOIN reservation ON rendered.ReservationID = reservation.ReservationID)'+
                ' INNER JOIN customer ON reservation.CustomerID = customer.CustomerID);'
*/


                break;

            case 'invoice':

                    // a.ReservationID, a.Invoice_Total, b.Reservation_Date, d.CustomerID, d.First_Name, d.Last_Name, cabins, services

                sql = 'SELECT * FROM invoice AS a'+
                ' INNER JOIN reservation AS b ON a.ReservationID=b.ReservationID'+
                ' INNER JOIN ( SELECT count(AccommodationID) AS cabins, ReservationID AS resID, CabinID as cabid FROM accommodation GROUP BY ReservationID ) AS b ON a.ReservationID=resID'+
                ' INNER JOIN ( SELECT count(RenderedID) AS services, ReservationID AS renID FROM rendered GROUP BY ReservationID ) AS c ON a.ReservationID=renID'+
                ' INNER JOIN customer AS d ON b.CustomerID=d.CustomerID'+
                ' INNER JOIN cabin AS f ON f.CabinID=cabid'+
                ' INNER JOIN cabin_type AS g ON f.Cabin_TypeID=g.Cabin_TypeID'+
                ' INNER JOIN branch AS e ON e.BranchID=f.BranchID'+
                ' WHERE b.Reservation_Date>"'+ftDate.from+'" AND b.Reservation_Date<"'+ftDate.to+'"';


                
                if ( branchID != 'Select branch') {
                    sql +=  ' AND e.BranchID="'+branchID+'"';
                }

                sql += ' GROUP BY invoiceID'

                body = {
                    category: cat,
                    data: [],
                    keys: [ 'ReservationID', 'Invoice_Total', 'Reservation_Date', 'CustomerID', 'First_Name', 'Last_Name', 'cabins', 'services', 'Branch_Name', 'options' ],
                }
                break;

            case 'services':

                sql = 'SELECT * FROM site AS a'+
                        ' INNER JOIN service AS b ON a.ServiceID=b.ServiceID'+
                        ' INNER JOIN branch AS c ON a.BranchID=c.BranchID'

                        if ( branchID != 'Select branch') {
                            sql +=  ' WHERE c.BranchID="'+branchID+'"';
                        }

                    body = {
                        category: cat,
                        data: [],
                        keys: [ 'ServiceID', 'Service_Name', 'Service_Price', 'Branch_Name', 'BranchID', 'option' ],
                    }

                break;

            case 'cabins':

                sql = 'SELECT * FROM cabin AS a'+
                        ' INNER JOIN cabin_type AS b ON a.Cabin_TypeID=b.Cabin_TypeID'+
                        ' INNER JOIN branch AS c ON a.BranchID=c.BranchID'

                if ( branchID != 'Select branch') {
                    sql +=  ' WHERE c.BranchID="'+branchID+'"';
                }

                body = {
                    category: cat,
                    data: [],
                    keys: [ 'CabinID', 'Cabin_Name', 'Type_Name', 'Cabin_Price', 'Party_Size', 'Branch_Name', 'option' ],

                }
                break;

            case 'customers':

                sql = 'SELECT * FROM customer AS a'+
                        ' INNER JOIN ( SELECT COUNT(ReservationID) AS Reservations, CustomerID AS cusID FROM reservation GROUP BY CustomerID ) AS b ON cusID=a.CustomerID'

                body = {
                    category: cat,
                    data: [],
                    keys: [ 'CustomerID', 'First_Name', 'Last_Name', 'Phone', 'Email', 'Street_Address', 'Reservations', 'options' ],
                }

                break;
            default:
                sql = 'SELECT * FROM '+cat;
                body = {
                    category: cat,
                    data: [],
                    keys: getKeys(cat),
                    headers: getHeaders(cat),
                }
                break;
        }

        maria = Maria(sql);
        maria.then(function(result) {
            body.data = result;

            resolve( body );
        });
    });
}

//################## PREPARE REPORT ENDS ##################//


//################## COMPONENT STARTS ##################//
function Component(cat, id) {
    return new Promise(function(resolve,reject) {

        console.log( '>>>>>>>>>>>>> '+cat );

        body = {
            category: [],
            data: [],
            keys: getKeys(cat),
            headers: getHeaders(cat),
        }
        switch (cat) {
            case 'branch':
                body.category = 'branch';
                break;
            case 'branches':
            body.category = 'branch';
                break;
            case 'accommodation':
            body.category = 'accommodation'
                break;
            case 'cabin':
            body.category = 'cabin'
                break;
            case 'cabin_type':
            body.category = 'cabin_type'
                break;
            case 'customer':
            body.category = 'customer'
                break;
            case 'invoice':
            body.category = 'invoice'
                break;
            case 'rendered':
            body.category = 'rendered'
                break;
            case 'reservation':
            body.category = 'reservation'
                sql = 'SELECT * FROM reservation'
                break;
            case 'service':
            body.category = 'service'
                break;
            case 'site':
            body.category = 'site'
                break;
            default:
                sql = '';
                break;
        }


        //sql = 'SELECT * FROM '+body.category;
        maria = Maria('SELECT * FROM '+cat);
        maria.then(function(result) {
            result = result;
            body.data = result;

            resolve(body);
        });
    });
}


//################## COMPONENT ENDS ##################//

//################## DELETE RECORD STARTS ##################//
// Create delete statement
function prepareSQLDelete(cat, id) {
    if ( cat == 'branches' ) {
        cat = 'branch';
    }
    itemID = getKeys(cat);
    sql = 'DELETE FROM '+cat+' WHERE '+itemID[0]+'="'+id+'";';

    return sql;

    /*
    switch (cat) {
        case 'branch':
            sql += 'BranchID='+id;
            break;
    }
    return sql;
    */
}


//################## DELETE RECORD ENDS ##################//



//################## FORM HADNLE STARTS ##################//
// Handle forms inserts and updates
function FormHandler(cat, data, isNew) {

    console.log( cat );

    if ( isNew == 'true' ) {
        return new Promise(function(resolve,reject) {
            maria = Maria(prepareSQLInsert(cat, data, isNew));
            maria.then(function(result) {
                resolve(result);
            });
        });
    } else {
        return new Promise(function(resolve,reject) {
            maria = Maria(prepareSQLUpdate(cat, data, isNew));
            maria.then(function(result) {
                if (result) {
                    resolve(result);
                } else {
                    reject('error');
                }

            });
        });
    }
}

// Prepare sql insert or update statement
// Insert statement uses prepareSQLInsertValues to finish the statement
function prepareSQLInsert(cat, data, isNew) {
    console.log( 'Insert into ' + cat);

    if ( cat == 'reservation' ) {
        return 'INSERT INTO '+cat+' VALUES ('+null+',"'+new Date().toISOString().slice(0,19).replace(/T/,' ')+'"'+prepareSQLInsertValues(cat, data.data)+');';
    } else {
        return 'INSERT INTO '+cat+' VALUES ('+null+''+prepareSQLInsertValues(cat, data.data)+');';
    }
}

// Get inserted values for SQL statement
// Global for all tables
function prepareSQLInsertValues(cat, data) {
    values = '';

    if ( cat == 'reservation' ) {
        for ( i = 2; i < data.length; i++ ) {
            values += ',"'+data[i]+'"';
        }
    } else {
        for ( i = 1; i < data.length; i++ ) {
            values += ',"'+data[i]+'"';
        }
    }

    return values;
}

// Prepare sql update statement
// Update statement uses prepareSQLUpdateValues to finish the statement
function prepareSQLUpdate(cat, data) {

    console.log(cat);
    sql = '';

    switch (cat) {
        case 'branch':
            sql = 'UPDATE branch SET '+prepareSQLUpdateValues(cat, data.data)+' WHERE BranchID="'+data.data[0]+'";';
            break;
        case 'accommodation':
            sql = 'UPDATE accommodation SET '+prepareSQLUpdateValues(cat, data.data)+' WHERE AccommodationID="'+data.data[0]+'";';
            break;
        case 'cabin':
            sql = 'UPDATE cabin SET '+prepareSQLUpdateValues(cat, data.data)+' WHERE CabinID="'+data.data[0]+'";';
            break;
        case 'cabin_type':
            sql = 'UPDATE '+cat+' SET '+prepareSQLUpdateValues(cat, data.data)+' WHERE Cabin_TypeID="'+data.data[0]+'";';
            break;
        case 'customer':
            sql = 'UPDATE '+cat+' SET '+prepareSQLUpdateValues(cat, data.data)+' WHERE CustomerID="'+data.data[0]+'";';
            break;
        case 'invoice':
            sql = 'UPDATE '+cat+' SET '+prepareSQLUpdateValues(cat, data.data)+' WHERE InvoiceID="'+data.data[0]+'";';
            break;
        case 'rendered':
            sql = 'UPDATE '+cat+' SET '+prepareSQLUpdateValues(cat, data.data)+' WHERE RenderedID="'+data.data[0]+'";';
            break;
        case 'reservation':
            sql = 'UPDATE '+cat+' SET '+prepareSQLUpdateValues(cat, data.data)+' WHERE ReservationID="'+data.data[0]+'";';
            break;
        case 'service':
            sql = 'UPDATE '+cat+' SET '+prepareSQLUpdateValues(cat, data.data)+' WHERE ServiceID="'+data.data[0]+'";';
            break;
        case 'site':
            sql = 'UPDATE '+cat+' SET '+prepareSQLUpdateValues(cat, data.data)+' WHERE SiteID="'+data.data[0]+'";';
            break;
        default:
            sql = 'error: prepareSQLUpdate()';
            break;
    }
    return sql;
}

// Returns keys matched with correct values for prepareSQLUpdate
// Global for every table
function prepareSQLUpdateValues(cat, data) {
    switch (cat) {
        case 'branch':
            break;
        case 'accommodation':
            break;
        case 'cabin':
            break;
        case 'cabin_type':
            break;
        case 'customer':
            break;
        case 'invoice':
            break;
        case 'rendered':
            break;
        case 'reservation':
            break;
        case 'service':
            break;
        case 'site':
            break;
        default:
            keys = 'error';
    }

        keys = getKeys(cat);
        values = '';

            for ( i = 1; i < keys.length; i++ ) {
                if ( i == keys.length - 1 ) {
                    values += keys[i]+'="'+data[i]+'"';
                } else {
                    values += keys[i]+'="'+data[i]+'",';
                }
            }


    return values;
}


//################## FORM HADNLE ENDS ##################//

//################## FORM STARTS ##################//
// Prepare form and send it to client
// If ID is not present, send empty form.
// Otherwise get data from database with matching ID
function EditItem(cat, id) {
    return new Promise(function(resolve,reject) {
        body = {
            isEmpty: true,
            category: '',
            headers: getHeaders(cat),
            keys: getKeys(cat),
            data: '',
        }
        switch (cat) {
            case 'branch':
                body.category = 'branch',
                sql = 'SELECT * FROM branch WHERE BranchID='+id
                break;
            case 'accommodation':
                body.category = 'accommodation',
                sql = 'SELECT * FROM accommodation WHERE AccommodationID='+id
                break;
            case 'cabin':
                body.category = 'cabin',
                sql = 'SELECT * FROM cabin WHERE CabinID='+id
                break;
            case 'cabin_type':
                body.category = 'cabin_type',
                sql = 'SELECT * FROM cabin_type WHERE Cabin_TypeID='+id
                break;
            case 'customer':
                body.category = 'customer',
                sql = 'SELECT * FROM customer WHERE CustomerID='+id
                break;
            case 'invoice':
                body.category = 'invoice',
                sql = 'SELECT * FROM invoice WHERE InvoiceID='+id
                break;
            case 'rendered':
                body.category = 'rendered',
                sql = 'SELECT * FROM rendered WHERE RenderedID='+id
                break;
            case 'reservation':
                body.category = 'reservation',
                sql = 'SELECT * FROM reservation WHERE ReservationID='+id
                break;
            case 'service':
                body.category = 'service',
                sql = 'SELECT * FROM service WHERE ServiceID='+id
                break;
            case 'site':
                body.category = 'site',
                sql = 'SELECT * FROM site WHERE SiteID='+id
                break;
            default:
                sql = '';
                break;
        }

        if (id != 0) {
            maria = Maria(sql);
            maria.then(function(result) {
                body.data = result;
                body.isEmpty = false;
                resolve(body);
            });
        } else {
            resolve(body);
        }
    });
}

//################## FORM ENDS ##################//



//################## MANAGEMENT LIST STARTS ##################//
// Prepare response body
// Get headers, keys and data
function Management(cat, id) {
    return new Promise(function(resolve,reject) {
        body = {
            category: '',
            headers: getHeaders(cat),
            keys: getKeys(cat),
            data: [],
        }

        switch (cat) {
            case 'branches':
                body.category = 'branch'
                break;
            case 'accommodation':
                body.category = 'accommodation'
                break;
            case 'cabin':
                body.category = 'cabin'
                break;
            case 'cabin_type':
                body.category = 'cabin_type'
                break;
            case 'customer':
                body.category = 'customer'
                break;
            case 'invoice':
                body.category = 'invoice'
                break;
            case 'rendered':
                body.category = 'rendered'
                break;
            case 'reservation':
                body.category = 'reservation'
                break;
            case 'service':
                body.category = 'service'
                break;
            case 'site':
                body.category = 'site'
                break;
        }

        getList = getListData(cat, id);
        getList.then(function(result) {
            body.data = result;
            resolve(body);
        });
    });
}

// Get data for list
// Prepare sql statement and pass it to Maria
function getListData(cat, id) {
    return new Promise(function(resolve,reject) {
        maria = Maria(prepareSQLList(cat, id));
        maria.then(function(result) {
            resolve(result);
        });
    });
}

// Prepare sql statement
// If ID is given, get only one record
// Else get whole list
function prepareSQLList(cat, id) {
    if (cat == 'branches') {
        cat = 'branch';
    }
    itemID = getKeys(cat);

    if ( cat == 'reservation' && id != '' ) {
        return 'SELECT * FROM reservation WHERE ReservationID='+id+' ORDER BY Reservation_Date';
    } else if ( cat == 'reservation' && id == '' ) {
        return 'SELECT * FROM reservation ORDER BY Reservation_Date DESC';
    }


    if ( id != '' ) {
        return 'SELECT * FROM '+cat+' WHERE '+itemID[0]+'="'+id+'";';
    } else {
        return 'SELECT * FROM '+cat;
    }


/*
    switch (cat) {
        case 'branches':
            if (id != '') {
                return 'SELECT * FROM branch WHERE BranchID="'+id+'";';
            } else {
                return 'SELECT * FROM branch;';
            }
        case 'accommodation':
            if (id != '') {
                return 'SELECT * FROM accommodation WHERE BranchID="'+id+'";';
            } else {
                return 'SELECT * FROM accommodation;';
            }   
        default:
            return 'SELECT * FROM '+cat;
    }
*/


}

// Execute sql statement and return result
function Maria(sql) {
    return new Promise(function(resolve,reject) {
        conn.query(sql, function(err,res) {
            if (err) {
                console.log("\x1b[31m", sql);
                console.log( err );
                resolve(err);
            } else {
                console.log("\x1b[32m", sql);
                resolve(res);
            }
        });
    });
}

//################## MANAGEMENT LIST ENDS ##################//















