-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for reservations
CREATE DATABASE IF NOT EXISTS `reservations` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `reservations`;

-- Dumping structure for table reservations.accommodation
CREATE TABLE IF NOT EXISTS `accommodation` (
  `AccommodationID` int(11) NOT NULL AUTO_INCREMENT,
  `Nights` int(11) NOT NULL,
  `CheckIn` datetime DEFAULT NULL,
  `CheckOut` datetime DEFAULT NULL,
  `CabinID` int(11) NOT NULL,
  `ReservationID` int(11) NOT NULL,
  PRIMARY KEY (`CabinID`,`ReservationID`),
  UNIQUE KEY `AccommodationID` (`AccommodationID`),
  KEY `ReservationID` (`ReservationID`),
  CONSTRAINT `accommodation_ibfk_1` FOREIGN KEY (`CabinID`) REFERENCES `cabin` (`CabinID`),
  CONSTRAINT `accommodation_ibfk_2` FOREIGN KEY (`ReservationID`) REFERENCES `reservation` (`ReservationID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table reservations.branch
CREATE TABLE IF NOT EXISTS `branch` (
  `BranchID` int(11) NOT NULL AUTO_INCREMENT,
  `Branch_Name` varchar(50) NOT NULL,
  `Branch_Address` varchar(50) NOT NULL,
  `Branch_Phone` varchar(50) NOT NULL,
  `Branch_Email` varchar(50) NOT NULL,
  PRIMARY KEY (`BranchID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table reservations.cabin
CREATE TABLE IF NOT EXISTS `cabin` (
  `CabinID` int(11) NOT NULL AUTO_INCREMENT,
  `Cabin_Name` varchar(50) NOT NULL,
  `Cabin_Price` double(6,2) NOT NULL,
  `BranchID` int(11) NOT NULL,
  `Cabin_TypeID` int(11) NOT NULL,
  PRIMARY KEY (`CabinID`),
  KEY `BranchID` (`BranchID`),
  KEY `TypeID` (`Cabin_TypeID`),
  CONSTRAINT `cabin_ibfk_1` FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`),
  CONSTRAINT `cabin_ibfk_2` FOREIGN KEY (`Cabin_TypeID`) REFERENCES `cabin_type` (`Cabin_TypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table reservations.cabin_type
CREATE TABLE IF NOT EXISTS `cabin_type` (
  `Cabin_TypeID` int(11) NOT NULL AUTO_INCREMENT,
  `Type_Name` varchar(50) NOT NULL,
  `Party_Size` int(11) NOT NULL,
  PRIMARY KEY (`Cabin_TypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table reservations.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `CustomerID` int(11) NOT NULL AUTO_INCREMENT,
  `Courtesy_Title` varchar(5) NOT NULL,
  `First_Name` varchar(50) NOT NULL,
  `Last_Name` varchar(50) NOT NULL,
  `SSN` varchar(20) NOT NULL,
  `Street_Address` varchar(50) NOT NULL,
  `City` varchar(50) NOT NULL,
  `ZIP` varchar(10) NOT NULL,
  `Country` varchar(50) NOT NULL,
  `Phone` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  PRIMARY KEY (`CustomerID`),
  UNIQUE KEY `CustomerIndex` (`SSN`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table reservations.invoice
CREATE TABLE IF NOT EXISTS `invoice` (
  `InvoiceID` int(11) NOT NULL AUTO_INCREMENT,
  `ReservationID` int(11) NOT NULL,
  `Invoice_Total` double(7,2) NOT NULL,
  PRIMARY KEY (`InvoiceID`),
  KEY `FK_invoice_reservation` (`ReservationID`),
  CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`ReservationID`) REFERENCES `reservation` (`ReservationID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table reservations.rendered
CREATE TABLE IF NOT EXISTS `rendered` (
  `RenderedID` int(11) NOT NULL AUTO_INCREMENT,
  `Rendered_Date` date NOT NULL,
  `Service_Amount` int(11) NOT NULL,
  `ServiceID` int(11) NOT NULL,
  `ReservationID` int(11) NOT NULL,
  PRIMARY KEY (`ServiceID`,`ReservationID`),
  UNIQUE KEY `RenderedID` (`RenderedID`),
  KEY `ReservationID` (`ReservationID`),
  CONSTRAINT `rendered_ibfk_1` FOREIGN KEY (`ServiceID`) REFERENCES `service` (`ServiceID`),
  CONSTRAINT `rendered_ibfk_2` FOREIGN KEY (`ReservationID`) REFERENCES `reservation` (`ReservationID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table reservations.reservation
CREATE TABLE IF NOT EXISTS `reservation` (
  `ReservationID` int(11) NOT NULL AUTO_INCREMENT,
  `Reservation_Date` datetime NOT NULL,
  `CustomerID` int(11) NOT NULL,
  PRIMARY KEY (`ReservationID`),
  KEY `CustomerID` (`CustomerID`),
  CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table reservations.service
CREATE TABLE IF NOT EXISTS `service` (
  `ServiceID` int(11) NOT NULL AUTO_INCREMENT,
  `Service_Name` varchar(50) NOT NULL,
  PRIMARY KEY (`ServiceID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table reservations.site
CREATE TABLE IF NOT EXISTS `site` (
  `SiteID` int(11) NOT NULL AUTO_INCREMENT,
  `Service_Price` double(6,2) NOT NULL,
  `ServiceID` int(11) NOT NULL,
  `BranchID` int(11) NOT NULL,
  PRIMARY KEY (`ServiceID`,`BranchID`),
  UNIQUE KEY `SiteID` (`SiteID`),
  KEY `BranchID` (`BranchID`),
  CONSTRAINT `site_ibfk_1` FOREIGN KEY (`ServiceID`) REFERENCES `service` (`ServiceID`),
  CONSTRAINT `site_ibfk_2` FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
