


$('[data-toggle="datepicker"]').datepicker({
    format: 'yyyy-mm-dd'
});



curTable = null;
// Search from table
function searchFromList() {
    key = $('#selectSearchKey').val();
    value = $('#searchFieldID').val();
    url = 'api/search/'+curTable+'?key='+key+'&value='+value;


    $.ajax({
        type: "GET",
        url: url,
        data: {},
        dataType: "json",
        success: function (response) {
            
            console.log( response );
            buildTable( response );

            listData = response;

            if (response.data.length == 0) {
                message('no records found!');
            }
        }
    });
}

function setSearchCriteria( value ) {
    $('#searchFieldID').val( value );

}


listData = null;
// Table onclick get list data
function getList(cat) {

    id = $('#searchFieldID').val();

    url = 'api/management/'+cat+'/?id=';

    $.ajax({
        type: "GET",
        url: url,
        data: "data",
        dataType: "json",
        success: function (response) {
            buildTable( response );

            listData = response;
            message(response);
        }
    });

}


// Build the table
function buildTable(data) {
    tableContext = '';
    tableContext += getTableHeaders(data.headers);
    tableContext += getTableItems(data.data, data.keys, data.category);

    $('#tableData').html(tableContext);


    searchKeys = '';
    for ( i in data.headers ) {
        searchKeys += '<option>'+data.headers[i]+'</option>'
    }

    curTable = data.category;
    $('#selectSearchKey').html( searchKeys );
}

// Parse headers
function getTableHeaders(headers) {
    headersContext = '<tr>';
        for ( i in headers ) {

            if ( headers[i].match(/ID/) ) {
                cellWidth = '';
            } else {
                cellWidth = 'auto';
            }

            headersContext += '<th onclick="sortReport(0,\''+headers[i]+'\')" style="width:'+cellWidth+';">'+headers[i]+'</th>'
        }
        headersContext += '<th>options</th>';
    headersContext += '</tr>';
    return headersContext;
}

// Parse table items
function getTableItems(item, keys, cat) {
    itemsContext = '';
        for ( i in item ) {
            if ( i % 2 == 0 ) {
                rowCol = 'rgb(230,230,230)';
            } else {
                rowCol = 'rgb(250,250,250)';
            }




            itemsContext += '<tr style="background:'+rowCol+'">';
            for ( j in keys ) {

                if ( keys[j] == 'Reservation_Date' || keys[j] == 'Rendered_Date' || keys[j] == 'CheckIn' || keys[j] == 'CheckOut' ) {
                    reDate = item[i][keys[j]].replace(/T/,' ').slice(0, this.length - 2);
                    sDate = item[i][keys[j]].replace(/T/,' ').slice(0, this.length - 14);
                    itemsContext += '<td onclick="setSearchCriteria(\''+sDate+'\')">'+reDate+'</td>'

                } else {
                    itemsContext += '<td onclick="setSearchCriteria(\''+item[i][keys[j]]+'\')">'+item[i][keys[j]]+'</td>'
                }    

            }

            itemsContext += '<td><a href="#" onclick="editItem('+item[i][keys[0]]+',\''+cat+'\')">edit</a> | <a href="#" onclick="deleteItem('+item[i][keys[0]]+',\''+cat+'\')">delete</a></td>'

            itemsContext += '</tr>';
        }
    return itemsContext;
}

// Delete item
function deleteItem(item, cat) {
    url = 'api/delete/'+cat+'?id='+item;

    $.ajax({
        type: "POST",
        url: url,
        data: "data",
        dataType: "html",
        success: function (response) {

            
            message(response);
        }
    });
}

// Edit item
function editItem(item, cat) {

    console.log( cat );

    url = 'api/edit/'+cat+'?id='+item;

    console.log( url );

    $.ajax({
        type: "GET",
        url: url,
        data: {},
        dataType: "json",
        success: function (response) {
            buildForm(response);


            message(response);

        }
    });
}

// Build form
function buildForm(data) {
    formContext = '';
    formContext += getFormRows(data.headers, data.keys, data.data, data.category, data.isEmpty);

    console.log( '>>>>>>>>>>>>>> ' + data.category );

    $('#formPanel').html(formContext);
}

// Parse form rows
// Creates dropdown for Foreign Key values
// Create input field for CustomerID AND ReservationID
// For cleaner usage example when Customer count is high
function getFormRows(headers, keys, data, category, isEmpty) {
    rowContext = '';
    fieldIDs = [];
    isEmpty = isEmpty;
    cat = category;
    dDC = 0;
        for ( i in headers ) {
            label = headers[i];
            fieldID = ['formField_'+keys[i]];

            if (isEmpty == true) {
                fieldData = '';
            } else {
                fieldData = data[0][keys[i]];
            }
            fieldIDs.push( fieldID );


            res = keys[i].match(/ID/);
            


            if ( res != null && i > 0 && keys[i] != 'CustomerID' && keys[i] != 'ReservationID' ) {
                dDC++;
                testKey = keys[i].toLowerCase().slice(0, length - 2);
                console.log( testKey );
                rowContext += '<li><span>'+label+'</span><select id="'+fieldID+'">'+getComponent( testKey, dDC, keys[i], fieldData )+'</select></li>'
            } else {



                if ( isEmpty == false ) {
                    if ( keys[i] == 'Reservation_Date' || keys[i] == 'Rendered_Date' || keys[i] == 'CheckIn' || keys[i] == 'CheckOut' ) {
                        reDate = data[0][keys[i]].replace(/T/,' ').slice(0, this.length - 2);
                        rowContext += '<li><span>'+label+'</span><input data-toggle="datepicker" autocomplete="off" id="'+fieldID+'" class="formTxtField" placeholder="'+label+'..." value="'+reDate+'"></li>';

                    } else {
                        rowContext += '<li><span>'+label+'</span><input autocomplete="off" id="'+fieldID+'" class="formTxtField" placeholder="'+label+'..." value="'+fieldData+'"></li>';


                    }


                
                    } else if ( keys[i] == 'Reservation_Date' || keys[i] == 'Rendered_Date' || keys[i] == 'CheckIn' || keys[i] == 'CheckOut' ) {
                        rowContext += '<li><span>'+label+'</span><input data-toggle="datepicker" autocomplete="off" id="'+fieldID+'" class="formTxtField" placeholder="'+label+'..." value="'+fieldData+'"></li>';
                    } else {
                        rowContext += '<li><span>'+label+'</span><input autocomplete="off" id="'+fieldID+'" class="formTxtField" placeholder="'+label+'..." value="'+fieldData+'"></li>';

                    }


                if ( keys[i] == 'Reservation_Date' || keys[i] == 'Rendered_Date' || keys[i] == 'CheckIn' || keys[i] == 'CheckOut' ) {
                    $(document).on('click', '#'+fieldID, function() {
                        $('[data-toggle="datepicker"]').datepicker({
                            format: 'yyyy-mm-dd'
                        });
                    });
                }

                if ( keys[i] == 'CheckIn' || keys[i] == 'CheckOut' ) {
                    $(document).on('click', '#'+fieldID, function() {
                            ONE_DAY = 1000 * 60 * 60 * 24;
                            date1_ms = new Date($('#formField_CheckIn').val()).getTime();
                            date2_ms = new Date($('#formField_CheckOut').val()).getTime();
                            difference_ms = Math.abs(date1_ms - date2_ms);

                            $('#formField_Nights').val( parseInt( difference_ms/ONE_DAY ) );

                    });
                }
            

            }

        }



        rowContext += '<li><input id="formSubmitBtn" type="button" value="save" onclick="saveItem(\''+fieldIDs+'\',\''+cat+'\','+isEmpty+')"></li>';
        return rowContext;
}

// Settimeout to sort bug, if form have two dropdown menus
// First one will not work

function getComponent( cat, i, value, data ) {
    setTimeout(function() {
        cat = cat;

    //cat = cat.slice( 0, cat.length - 2 ).toLowerCase()
    url = 'api/component/'+cat;

    $.ajax({
        type: "GET",
        url: url,
        data: {},
        dataType: "json",
        success: function (response) {
            console.log( response );
            response = response
            buildDropDown2( response, value, data );
            message( response);
        }
    });

    }, 200 * i)
}

// Reporting
function buildDropDown2(data, value, dataValue) {

    data = data;

    dropDownContext = '<select>';
    dropDownContext += '<option>Select '+data.category+'</option>'
    for ( i in data.data ) {


        itemID = data.data[i][data.keys[0]]



        if ( data.keys[0] == 'ServiceID' || data.keys[0] == 'CabinID' || data.keys[0] == 'BranchID' || data.keys[0] == 'Cabin_TypeID' ) {
            itemName = data.data[i][data.keys[1]]
        } else {
            itemName = data.data[i][data.keys[0]]
        }


        dropDownContext += '<option value="'+itemID+'">'+itemName+'</option>'
    }
    dropDownContext += '</select>';
    //$('#formField_'+data.keys[0]).empty();

    $('#formField_'+data.keys[0]).html(dropDownContext);


// Proceed with care
// Warning!
    arr = data.data
    res = arr.find(function(element, index) {
        if ( element[value] == dataValue ) {
            $('#formField_'+data.keys[0]).val( data.data[index][value] );
        } else {
        }
    });
    //console.log( data.data[0][value] );

    
}




// Post form data to server
// Get new data after update
function saveItem(fieldIDs, cat, isEmpty) {
    url = 'api/edit/'+cat+'/?new='+isEmpty;
    data = {
        category: cat,
        data: getFormData(fieldIDs),
        }

    $.ajax({
        type: "POST",
        url: url,
        data: { data },
        dataType: "html",
        success: function (response) {
            message(response);
            getList( curTable );
        }
    });


}

// Get form fields values and return as array
function getFormData(data) {
    data = data.split(',');
    body = [];
    for ( i in data ) {
        body.push( $('#'+data[i]).val() );
    }

    return body;
}


// Get component data
function getComponentData() {
    url = 'api/component/branch';

    $.ajax({
        type: "GET",
        url: url,
        data: "data",
        dataType: "json",
        success: function (response) {
            
            buildDropDown( response );
            message(response);
        }
    });
}

// Reporting
function buildDropDown(data) {


    dropDownContext = '';
    dropDownContext += '<option>Select branch</option>'
    for ( i in data.data ) {
        branchID = data.data[i].BranchID;
        branchName = data.data[i].Branch_Name;

        dropDownContext += '<option value="'+branchID+'">'+branchName+'</option>'
    }
    $('#selectedBranch').html(dropDownContext);
}

let reportData = '';

// Get report
function getReport() {
    category = $('#reportCat').val();
    branchID = $('#selectedBranch').val();
    fromDate = $('#reportFromDate').val();
    toDate = $('#reportToDate').val();

    url = "api/report/?cat="+category+"&id="+branchID+"&from="+fromDate+"&to="+toDate;

    $.ajax({
        type: "GET",
        url: url,
        data: '',
        dataType: "json",
        success: function (response) {
            //message(response);

            console.log( response );

            buildReport( response );

            reportData = response;

        }
    });
}
// Build report
function buildReport( data ) {

    
    reportContext = '';
    reportContext += getReportHeaders(data.keys);

    reportContext += getReportItems(data.data,data.keys, data.category);

    $('#tableReport').html( reportContext );    
}

// Get headers for report
function getReportHeaders(headers) {
    headersContext = '<tr>';
        for ( i in headers ) {
            headersContext += '<th onclick="sortReport(1,\''+headers[i]+'\')">'+headers[i]+'</th>'
        }
    headersContext += '</tr>';

    return headersContext;
}

function getReportItems(data, keys, cat) {
    itemsContext = '';




    for ( i in data ) {
        if ( i % 2 == 0 ) {
            rowCol = 'rgb(230,230,230)';
        } else {
            rowCol = 'rgb(250,250,250)';
        }

        itemsContext += '<tr style="background:'+rowCol+';">';
        for ( j = 0; j < keys.length - 1; j++ ) {


            if ( keys[j] == 'Reservation_Date' || keys[j] == 'Rendered_Date' || keys[j] == 'CheckIn' || keys[j] == 'CheckOut' ) {
                reDate = data[i][keys[j]].replace(/T/,' ').slice(0, this.length - 2);
                itemsContext += '<td>'+reDate+'</td>'

            } else {
                itemsContext += '<td>'+data[i][keys[j]]+'</td>'
            }



        }

        if ( cat == 'invoice' ) {
            itemsContext += '<td><a href="#" onclick="showInvoice(0, '+data[i].ReservationID+')">show invoice</a> | <a href="#" onclick="showInvoice(1, '+data[i].ReservationID+')">update invoice total</a></td>'
        } else {
            itemsContext += '<td>-</td>'
        }

        itemsContext += '</tr>';
    }

    return itemsContext;
}

lastSort = null;
sortCounter = 0;
function sortReport(type, sortHeader) {

    console.log( type );

    if ( type == '0' ) {
        response = listData;
    } else if ( type == '1' ) {
        response = reportData;
    } else {

    }

    if ( lastSort != i ) {
        s = [-1,1,0];
        lastSort = i;
        sortCounter = 0;
    } else if ( sortCounter == 0 ) {
        s = [0,1,-1];
        sortCounter++;
    } else {
        s = [-1,1,0];
        lastSort = i;
        sortCounter = 0;
    }
    
        response.data.sort( function(a,b) {
            if ( a[sortHeader] > b[sortHeader] ) {
                return s[0];
            } else if ( a[sortHeader] > b[sortHeader] ) {
                return s[1];
            } else {
                return s[2];
            }
    
            //return a[i] - b[i];
        });

        
        if ( type == '0' ) {
            buildTable(response);
        } else if ( type == '1' ) {
            buildReport(response);
        } else {
    
        }
}


function showInvoice(type, resID) {

    url = 'api/invoice?id='+resID;

  //url = 'api/delete/'+cat+'?id='+item;

    $.ajax({
        type: "GET",
        url: url,
        data: "",
        dataType: "json",
        success: function (response) {
            

            buildInvoice( type, response );
        }
    });
}

// Build invoice
function buildInvoice(type, data) {

    $('#invoiceInfo').html( invoiceReservationInfo(data.customer.data[0], data.reservationKeys) );
    $('#invoiceCustomerInfo').html( invoiceCustomerInfo(data.titles, data.customer.data, data.customer.keys) );
    $('#invoiceBranchInfo').html( invoiceBranchInfo(data.accommodation[0], data.branchKeys) );

    $('#invoiceTable').html( buildInvoiceItems(data.accommodation, data.invoiceKeys, data.services, data.serviceKeys, data.accommodation[0].ReservationID, type ) );

    $('#invoicePanel').css('display', 'block');
}

function buildInvoiceItems(data, keys, serData, serKeys, resID, type ) {
    $('#invoiceTitle').html('Invoice for reservation '+data[0].ReservationID);
    invoiceContext = '';
    invoiceContext += '<tr><th>Description</th><th>Unit cost</th><th>Quantity</th><th>Amount</th></tr>';
    total = 0;

    for ( i in data ) {
        invoiceContext += '<tr>';
            for ( j in keys ) {
                invoiceContext += '<td>'+data[i][keys[j]]+'</td>';
            }
            price = Math.round(parseFloat( parseFloat(data[i][keys[1]]) * parseFloat(data[i][keys[2]]) * 100)) / 100;
            total += parseFloat( price );


            invoiceContext += '<td>'+price+'</td>';
        invoiceContext += '</tr>';
    }

    for ( i in serData ) {
        invoiceContext += '<tr>';
            for ( j in serKeys ) {
                invoiceContext += '<td>'+serData[i][serKeys[j]]+'</td>';
            }

            price = Math.round( parseFloat( parseFloat(serData[i][serKeys[1]]) * parseFloat(serData[i][serKeys[2]]) * 100)) / 100;
            total += parseFloat( price );

            invoiceContext += '<td>'+price+'</td>';
        invoiceContext += '</tr>';
    }

        console.log( resID, total );

        if ( type == '1') {
            updateInvoiceTotal( resID, total );
        }

    $('#invoiceTotal').html('Invoice total: '+total+'€');
    return invoiceContext;
}

function updateInvoiceTotal( resID, total ) {
    url = 'api/updateInvoice'    

    $.ajax({
        type: "POST",
        url: url,
        data: { ReservationID: resID, total: total },
        dataType: "html",
        success: function (response) {
            message( response );
        }
    });

}

function invoiceReservationInfo(data, keys) {
    reservationContext = '';


        reservationContext += '<li><h3>Reservation info</h3></li>'
    for ( i in keys ) {

        if ( keys[i] == 'Reservation_Date' ) {
            reDate = data[keys[i]].replace(/T/,' ').slice(0, this.length - 2);
            reservationContext += '<li><b>'+keys[i]+':</b> '+reDate+'</li>';

        } else {
            
            reservationContext += '<li><b>'+keys[i]+':</b> '+data[keys[i]]+'</li>';
        }
    }

    
    return reservationContext;
}


function invoiceCustomerInfo(titles, data, keys) {
    customerContext = '';

    for ( i in data ) {
        customerContext += '<li><h3>Customer info</h3></li>'
        for ( j in keys ) {
            customerContext += '<li><b>'+keys[j]+':</b> '+data[i][keys[j]]+'</li>';
        }
    }

    return customerContext;
}

function invoiceBranchInfo(data, keys) {
    branchContext = '';
        branchContext += '<li><h3>Branch info</h3></li>'
        for ( i in keys ) {
            branchContext += '<li><b>'+keys[i]+': </b>'+data[keys[i]]+'</li>'  
        }
    

    return branchContext;
}


// close invoice

$('#invoiceCloseBtn').click(function() {
    $('#invoicePanel').css('display', 'none');

}); 

// print invoice
// save as attachment
$('#invoicePrintBtn').click(function() {

    // "width=794,height=1123"

    $('#invoiceCloseBtn').css('display', 'none');
    $('#invoicePrintBtn').css('display', 'none');

    windowsFeatures = "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes";

    styleCSS = '* {    padding: 0;    margin: 0 auto;    list-style: none;    text-decoration: none;}body {    font-family: Arial;    background: #f7f7f7;    color: #444;    min-width: 794px;    max-width: 794px;    min-height: 1123px;    max-height: 1123px;}#invoicePanel {    float: left;    position: absolute;    width: 794px;    height: 1123px;    background: #fff;    border: 1px solid #ddd;    margin: 50px;    display: none;}#invoiceTitle, #invoiceCloseBtn, #invoicePrintBtn  {    float: left;    width: 100%;    height: 100px;    line-height: 100px;    text-align: center;    font-size: 24px;}#invoiceCloseBtn, #invoicePrintBtn {    font-size: 14px;    color: red;    height: 30px;    line-height: 30px;    cursor: pointer;}ul {    float: left;    width: 300px;    height: auto;    border-left: 4px solid rgb(100,100,255);    margin-left: 40px;    margin-top: 20px;    margin-right: 100px;}li {    float: left;    width: 290px;    height: 24px;    line-height: 24px;    margin-left: 10px;    font-size: 12px;}#invoiceInfo {}#invoiceCustomerInfo {}#invoiceBranchInfo {}#invoiceTablePanel {    float: left;    padding: 20px 0;    margin: 40px 0;    background: #eff7ef; padding-right: 30px; }#invoiceTable {    float: left;    border-collapse: collapse;    width: 734px;    margin-left: 30px;}#invoiceTable tr {    width: 734px;    height: 35px;    text-align: left;}#invoiceTable th {    border-bottom: 1px solid #333;    font-size: 13px;}#invoiceTable th, #invoiceTable td {    }#invoiceTable td {    font-size: 12px;}#invoiceTotal {    float: left;    text-align: right;    width: 604px;    margin-left: 30px;    font-size: 13px;    font-weight: bold;}';

    myWindow = window.open("/", "Invoice", windowsFeatures );
    myWindow.document.write( '<title>Invoice</title><style>'+styleCSS+'</style>' + $('#invoicePanel').html() + '' );

    $('#invoiceCloseBtn').css('display', 'block');
    $('#invoicePrintBtn').css('display', 'block');

});

displayView = 0;
function openReportPanel() {



    if ( displayView == 0 ) {
        displayView = 1;
        $('#reportingPanel').css('display', 'block');
        $('#managePanel').css('display', 'none');

        $('#reportToggleBtn').html('Back to management');
    } else {
        displayView = 0;
        $('#managePanel').css('display', 'block');
        $('#reportingPanel').css('display', 'none');

        $('#reportToggleBtn').html('View Reports');
    }
}


// Handle server response
function message(response) {

    if ( response == 'no records found!') {
        $('#messageBox').css( { 'display': 'block', 'background-color': 'rgb(200,100,100)' } );
        $('#messageBox').html( 'no records found!' );
    }

    try {
        response = JSON.parse(response);
        console.log( response.sqlMessage );
    } catch (err) {
        if (err) throw err;
    }

    console.log( response );



    if ( response.sqlMessage != null ) {
        $('#messageBox').css( { 'display': 'block', 'background-color': 'rgb(200,100,100)' } );
        $('#messageBox').html( response.sqlMessage );

    } else if ( response.affectedRows > 0 ) {
        $('#messageBox').css( { 'display': 'block', 'background-color': 'rgb(100,200,100)' } );
        $('#messageBox').html( 'Updated!' );

    } else {
        $('#messageBox').css('display', 'none');
    }
}

function showHelp(i) {
    switch (i) {
        case 0:
            txt =   '1. Click new reservation button.<br>'+
                    '2. Fill only CustomerID field.<br>'+
                    '3. Refresh reservation list and look for reservationID<br>'+
                    '4. Click new accommodation.<br>'+
                    '5. Add accommodations to reservation.<br>'+
                    '5. Click new rendered.<br>'+
                    '6. Add services to reservation.<br>'+
                    '7. And finish reservation by click new invoice.<br>';


            $('#messageBox').css( { 'display': 'block', 'background-color': 'rgb(100,100,200)' } );
            $('#messageBox').html( txt );
            break;


    }
}


$('#messageBox').click(function(e) {

    $('#messageBox').css('display', 'none');
});




// Page configuration stuff
// Get dropdown menu for reports
// Get branch list
getComponentData();
getList('branches');



$('#messageBox').css('display', 'none');
$('#reportingPanel').css('display', 'none');





